//
//  Functions.swift
//  Lugares
//
//  Created by joan mazo on 11/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import Foundation

func afterDelay(_ seconds: Double, closure: @escaping () -> ()) {
    
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: closure)
}

let applicationDocumentsDirectory: URL = {
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return urls[0]
}()

//handler CoreData Errors
let ManagedObjectContextSaveDidFailNotification = Notification.Name(rawValue: "ManagedObjectContextSaveDidFailNotification")

func fatalCoreDataError(_ error: Error) {
    print("Fatal CoreData error: \(error)")
    NotificationCenter.default.post(name: ManagedObjectContextSaveDidFailNotification, object: nil)
}
