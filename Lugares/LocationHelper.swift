//
//  LocationHelper.swift
//  Lugares
//
//  Created by joan mazo on 10/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import Foundation
import CoreLocation

private let dateToString: DateFormatter = {
    
    print("DateFormatter initialize")
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    
    return formatter
}()

class LocationHelper {
    
    class func convertCoordinatesToString(latitude: Double, longitude: Double) -> (lat: String, long: String) {
        
        let lat = String(format: "%.8f", latitude)
        let long = String(format: "%.8f", longitude)
        
        return (lat, long)
    }
    
    class func convertPlacemarkToString(placemark: CLPlacemark) -> String {
        var line1 = ""
        var line2 = ""
        
        //aditional street address
        if let s = placemark.subThoroughfare {
            line1 += s + " "
        }
        
        //street address
        if let s = placemark.thoroughfare {
            line1 += s
        }
        
        //city of address
        if let s = placemark.locality {
            line2 += s + " "
        }
        
        //departamento or province
        if let s = placemark.administrativeArea {
            line2 += s + ", "
        }
        
        if let s = placemark.country {
            line2 += s
        }
        
        return line1 + "\n" + line2
    }
    
    
    class func convertDateToString(date: Date) -> String {
        
        let dateformatter = dateToString
        return dateformatter.string(from: date)
    }
    
}










