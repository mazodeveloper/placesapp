//
//  TagLocationVC.swift
//  Lugares
//
//  Created by joan mazo on 10/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class TagLocationVC: UITableViewController {
    
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addPhotoLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    
    //titles
    @IBOutlet weak var latitudeKey: UILabel!
    @IBOutlet weak var longitudeKey: UILabel!
    @IBOutlet weak var addressKey: UILabel!
    @IBOutlet weak var dateKey: UILabel!
    
    var date = Date()
    var placemark: CLPlacemark?
    var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    var managedObjectContext: NSManagedObjectContext!
    var image: UIImage?
    var observer: Any!
    
    //Edit location
    var editLocation: Location?
    
    struct IDS {
        static let categoriesSegue = "CategoriesSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let location = editLocation {
            
            navigationController?.title = "Edit location"
            
            let coord = CLLocationCoordinate2D(
                latitude: location.latitude,
                longitude: location.longitude
            )
            coordinate = coord
            placemark = location.placemark
            date = location.date
            descriptionText.text = location.locationDescription
            categoryLabel.text = location.category
            
            //load image if location have a image
            if location.hasPhoto {
                if let locationImage = location.photoImage {
                    showPhoto(image: locationImage)
                }
            }
            
            
        }else {
            categoryLabel.text = "No Category"
            descriptionText.text = ""
        }
        
        let tupla = LocationHelper.convertCoordinatesToString(
            latitude: coordinate.latitude,
            longitude: coordinate.longitude
        )
        latitudeLabel.text = tupla.lat
        longitudeLabel.text = tupla.long
        if let placemark = placemark {
            addressLabel.text = LocationHelper.convertPlacemarkToString(placemark: placemark)
        }
        
        dateLabel.text = LocationHelper.convertDateToString(date: date)
        descriptionText.becomeFirstResponder()
        
        //Tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
        tapGesture.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapGesture)
        
        //listen the background event
        listenForBackgroundNotification()
        
        //TableView style
        tableView.backgroundColor = UIColor.black
        tableView.separatorColor = UIColor(white: 1.0, alpha: 0.2)
        tableView.indicatorStyle = .white
        
        descriptionText.textColor = UIColor.white
        descriptionText.backgroundColor = UIColor.black
        
        categoryLabel.textColor = UIColor.white
        categoryLabel.highlightedTextColor = categoryLabel.textColor
        
        addPhotoLabel.textColor = UIColor.white
        addPhotoLabel.highlightedTextColor = addPhotoLabel.textColor
        
        addressLabel.textColor = UIColor.white
        addressLabel.highlightedTextColor = addressLabel.textColor
        addressKey.textColor = UIColor.white
        addressKey.highlightedTextColor = addressKey.textColor
        
        latitudeKey.textColor = UIColor.white
        latitudeKey.highlightedTextColor = latitudeKey.textColor
        latitudeLabel.textColor = UIColor.white
        latitudeLabel.highlightedTextColor = latitudeLabel.textColor
        
        longitudeKey.textColor = UIColor.white
        longitudeKey.highlightedTextColor = longitudeKey.textColor
        longitudeLabel.textColor = UIColor.white
        longitudeLabel.highlightedTextColor = longitudeLabel.textColor
        
        dateKey.textColor = UIColor.white
        dateKey.highlightedTextColor = dateKey.textColor
        dateLabel.textColor = UIColor.white
        dateLabel.highlightedTextColor = dateLabel.textColor
    }
    
    deinit {
        print("******** deinit correct ********")
        NotificationCenter.default.removeObserver(observer)
    }
    
    func closeKeyboard(_ gestureRecognizer: UIGestureRecognizer) {
        
        let point = gestureRecognizer.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: point)
        
        if indexPath != nil && indexPath!.section == 0 && indexPath!.row == 0 {
            return
        }
        
        descriptionText.resignFirstResponder()
    }
    
    @IBAction func pressCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func pressDone(_ sender: Any) {
        let hudView = HudView.hud(inView: navigationController!.view, animated: true)
        
        let location: Location
        if let tmp = editLocation {
            location = tmp
            hudView.text = "Updated"
        }else {
            location = Location(context: managedObjectContext)
            location.photoID = nil
            hudView.text = "Tagged"
        }
        
        location.latitude = coordinate.latitude
        location.longitude = coordinate.longitude
        location.category = categoryLabel.text!
        location.locationDescription = descriptionText.text!
        location.date = date
        location.placemark = placemark
        
        //if the user choose a image try to save
        if let image = image {
            if !location.hasPhoto {
                location.photoID = Location.nextPhotoID() as NSNumber
            }
            
            if let data = UIImageJPEGRepresentation(image, 0.5) {
                do {
                    try data.write(to: location.photoURL, options: .atomic)
                }catch let error {
                    print(error)
                    fatalError(error.localizedDescription)
                }
            }
        }
        
        do {
            
            try managedObjectContext.save()
            afterDelay(0.6, closure: { [unowned self] in
                self.dismiss(animated: true, completion: nil)
            })
        
        }catch let error {
            fatalCoreDataError(error)
        }
    }
    
}

//MARK: -> UITableViewDelegate y prepare delegate
extension TagLocationVC {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //image picker
        if indexPath.section == 1 && indexPath.row == 0 {
            pickPhoto()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            return 88
        }else if indexPath.section == 1 && indexPath.row == 0 {
            if locationImage.isHidden {
                return 44
            }else {
                return 280
            }
            
        }else if indexPath.section == 2 && indexPath.row == 2{
            
            //addressLabel.frame.size = CGSize(width: view.bounds.size.width - 110, height: 1000)
            addressLabel.sizeToFit()
            
            return addressLabel.frame.size.height + 20
        }else {
            return 44
        }
    }
    
    //Se ejecutara antes de mostrar cada una de las celdas
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = UIColor.black
        
        let selectionView = UIView(frame: CGRect.zero)
        selectionView.backgroundColor = UIColor(white: 1.0, alpha: 0.2)
        cell.selectedBackgroundView = selectionView
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.categoriesSegue {
            let vc = segue.destination as! PickerCategoriesVC
            vc.selectedCategoryName = categoryLabel.text!
            vc.delegate = self
        }
    }
    
}

//MARK: -> PickerCategoriesDelegate
extension TagLocationVC: PickerCategoriesVCDelegate {
    
    func pickerCategoriesDelegate(_ controller: PickerCategoriesVC, categorySelected: String) {
        
        categoryLabel.text = categorySelected
        controller.navigationController?.popViewController(animated: true)
    }
    
}


//MARK: -> UIImagePickerControllerDelegate
extension TagLocationVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        image = info[UIImagePickerControllerEditedImage] as? UIImage
        if let tmp = image {
            showPhoto(image: tmp)
            tableView.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func showPhoto(image: UIImage) {
        locationImage.image = image
        locationImage.isHidden = false
        locationImage.frame = CGRect(x: 10, y: 10, width: 260, height: 260)
        locationImage.layer.cornerRadius = 10
        addPhotoLabel.isHidden = true
    }
    
    func pickPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            showPhotoMenu()
        }else {
            choosePhotoFromLibrary()
        }
    }
    
    func takePhotoWithCamara() {
        let imagePicker = MyImagePickerController()
        imagePicker.view.tintColor = view.tintColor
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func choosePhotoFromLibrary() {
        let imagePicker = MyImagePickerController()
        imagePicker.view.tintColor = view.tintColor
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func showPhotoMenu() {
        let alert = UIAlertController(title: "select photo", message: "Can take a photo or select an existent photo in the library", preferredStyle: .actionSheet)
        
        let photoLibraryAction = UIAlertAction(title: "Photo library", style: .default, handler: {[unowned self] (action) in
            self.choosePhotoFromLibrary()
        })
        
        let takePhotoAction = UIAlertAction(title: "Camara", style: .default, handler: {[unowned self] (action) in
            self.takePhotoWithCamara()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(photoLibraryAction)
        alert.addAction(takePhotoAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
}


//MARK: -> Notification center
extension TagLocationVC {
    
    func listenForBackgroundNotification() {
        observer = NotificationCenter.default.addObserver(
            forName: Notification.Name.UIApplicationDidEnterBackground,
            object: nil,
            queue: OperationQueue.main) {[unowned self] (notification) in
                
                if self.presentedViewController != nil {
                    self.dismiss(animated: true, completion: nil)
                }
                
                self.descriptionText.resignFirstResponder()
        }
    }
    
}














