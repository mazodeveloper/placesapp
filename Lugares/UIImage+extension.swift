//
//  UIImage+extension.swift
//  Lugares
//
//  Created by joan mazo on 16/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func resizedImage(withBounds bounds: CGSize) -> UIImage {
        
        //the size represent the real size of the image
        let horizontalRatio = bounds.width / size.width
        let verticalRatio = bounds.height / size.height
        
        let ratio = min(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
