//
//  ViewController.swift
//  Lugares
//
//  Created by joan mazo on 9/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import QuartzCore
import AudioToolbox

class CurrentLocationVC: UIViewController, CAAnimationDelegate {
    
    @IBOutlet weak var statusMessage: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var tagLocationButton: UIButton!
    @IBOutlet weak var getLocationButton: UIButton!
    
    //keys
    @IBOutlet weak var latitudeKey: UILabel!
    @IBOutlet weak var longitudeKey: UILabel!
    @IBOutlet weak var addressKey: UILabel!
    
    //current location in coordinates
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var updatingLocation = false
    var lastLocationError: Error?
    
    //reverse geocoder convert coordinate to address
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var performingReverseGeocoding = false
    var lastGeocodingError: Error?
    
    //timer
    var timer: Timer?
    
    //managedObjectContext
    var managedObjectContext: NSManagedObjectContext!
    
    //viewContainer and Icon
    @IBOutlet weak var containerView: UIView!
    var logoVisible = false
    
    //Sound
    var soundID: SystemSoundID = 0
    
    lazy var logoButton: UIButton = { [unowned self] in
        
        let button = UIButton(type: .custom)
        button.setBackgroundImage(UIImage(named: "Logo"), for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: #selector(getCurrentLocation), for: .touchUpInside)
        button.center.x = self.view.bounds.midX
        button.center.y = self.containerView.bounds.midY
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateLabels()
        loadSoundEffect("Sound.caf")
    }
    
    func showLogoView() {
        if !logoVisible {
            logoVisible = true
            containerView.isHidden = true
            view.addSubview(logoButton)
        }
    }
    
    func hideLogoView() {
        
        if !logoVisible {return}
        
        logoVisible = false
        containerView.isHidden = false
        containerView.center.x = self.view.bounds.size.width * 2
        containerView.center.y = 40 + containerView.bounds.size.height / 2
        
        let centerX = view.bounds.midX
        
        let panelMover = CABasicAnimation(keyPath: "position")
        panelMover.isRemovedOnCompletion = false
        panelMover.fillMode = kCAFillModeForwards
        panelMover.duration = 0.6
        panelMover.fromValue = NSValue(cgPoint: containerView.center)
        panelMover.toValue = NSValue(cgPoint: CGPoint(x: centerX, y: containerView.center.y))
        panelMover.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        panelMover.delegate = self
        containerView.layer.add(panelMover, forKey: "panelMover")
        
        let logoMover = CABasicAnimation(keyPath: "position")
        logoMover.isRemovedOnCompletion = false
        logoMover.fillMode = kCAFillModeForwards
        logoMover.duration = 0.5
        logoMover.fromValue = NSValue(cgPoint: logoButton.center)
        logoMover.toValue = NSValue(cgPoint: CGPoint(x: -centerX, y: logoButton.center.y))
        logoMover.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        logoButton.layer.add(logoMover, forKey: "logoMover")
        
        let logoRotator = CABasicAnimation(keyPath: "transform.rotation.z")
        logoRotator.isRemovedOnCompletion = false
        logoRotator.fillMode = kCAFillModeForwards
        logoRotator.duration = 0.5
        logoRotator.fromValue = 0.0
        logoRotator.toValue = -2 * Double.pi
        logoRotator.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        logoButton.layer.add(logoRotator, forKey: "logoRotator")
    }
    
    //Animation delegate method
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        containerView.layer.removeAllAnimations()
        containerView.center.x = view.bounds.size.width / 2
        containerView.center.y = 40 + containerView.bounds.size.height / 2
        
        logoButton.layer.removeAllAnimations()
        logoButton.removeFromSuperview()
    }
    
    @IBAction func getCurrentLocation(_ sender: UIButton) {
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            
            locationManager.requestWhenInUseAuthorization()
            return
            
        }else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted {
            showAlert(title: "Permisos denegados", message: "Querido usuario si desea usar nuestras servicios debe ir a settings y otorgarnos los permisos correspondientes.")
            return
        }
        
        if logoVisible {
            hideLogoView()
        }
        
        currentLocation = nil
        placemark = nil
        lastGeocodingError = nil
        
        if !updatingLocation {
            lastLocationError = nil
            startLocationManager()
            updateLabels()
            updateGetLocationButton()
        }else {
            stopLocationManager()
            updateLabels()
            updateGetLocationButton()
        }
        
    }
    
    func startLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            //una distancia aceptable sera una menor o igual a 10 metros
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            updatingLocation = true
            
            timer = Timer.scheduledTimer(
                timeInterval: 60,
                target: self,
                selector: #selector(didTimeOut),
                userInfo: nil,
                repeats: false
            )
        }
    }
    
    func stopLocationManager() {
        if updatingLocation {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            updatingLocation = false
            
            if let timer = timer {
                timer.invalidate()
            }
        }
    }
    
    func didTimeOut() {
        if currentLocation == nil {
            stopLocationManager()
            lastLocationError = NSError(domain: "MyLocationsErrorDomain", code: 1, userInfo: nil)
            updateLabels()
            updateGetLocationButton()
        }
    }
    
    func updateLabels() {
        if let location = currentLocation {
            let tupla = LocationHelper.convertCoordinatesToString(
                latitude: location.coordinate.latitude,
                longitude: location.coordinate.longitude
            )
            
            latitudeKey.isHidden = false
            longitudeKey.isHidden = false
            addressKey.isHidden = false
            latitudeLabel.isHidden = false
            longitudeLabel.isHidden = false
            addressLabel.isHidden = false
            tagLocationButton.isHidden = false
            
            latitudeLabel.text = tupla.lat
            longitudeLabel.text = tupla.long
            statusMessage.text = "Current location"
            
            //Placemark
            if let placemark = placemark {
                addressLabel.text = LocationHelper.convertPlacemarkToString(placemark: placemark)
            }else if performingReverseGeocoding {
                addressLabel.text = "Searching for address..."
            }else if lastGeocodingError != nil{
                addressLabel.text = "Error finding address"
            }else {
                addressLabel.text = "No address found"
            }
            
        }else {
            latitudeLabel.text = ""
            longitudeLabel.text = ""
            addressLabel.text = ""
            
            latitudeKey.isHidden = true
            longitudeKey.isHidden = true
            addressKey.isHidden = true
            latitudeLabel.isHidden = true
            longitudeLabel.isHidden = true
            addressLabel.isHidden = true
            tagLocationButton.isHidden = true
            
            var message: String
            if let error = lastLocationError as NSError? {
                if error.domain == kCLErrorDomain && error.code == CLError.denied.rawValue {
                    message = "Location services Disabled"
                }else {
                    message = "Error getting location"
                }
            }else if !CLLocationManager.locationServicesEnabled() {
                message = "Location services Disabled"
            }else if updatingLocation {
                message = "Searching..."
            }else {
                message = "Press 'Get location' to start"
                showLogoView()
            }
            statusMessage.text = message
        }
    }
    
    func updateGetLocationButton() {
        
        let spinnerTag = 1000
        
        if updatingLocation {
            getLocationButton.setTitle("Stop", for: .normal)
            if view.viewWithTag(spinnerTag) == nil {
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
                spinner.center = statusMessage.center
                spinner.center.y += spinner.bounds.size.height / 2 + 15
                spinner.startAnimating()
                spinner.tag = spinnerTag
                
                containerView.addSubview(spinner)
            }
            
        }else {
            getLocationButton.setTitle("Get location", for: .normal)
            if let spinner = view.viewWithTag(spinnerTag) {
                spinner.removeFromSuperview()
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
}

//MARK: -> CLLocationManagerDelegate
extension CurrentLocationVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Ha ocurrido un error \(error)")
        
        //si el error es de location desconocida no paramos el location manager, simplemente esperamos un nuevo update hasta que encontremos una location correcta. Si es otro tipo de error frenamos el locationManager
        if (error as NSError).code == CLError.locationUnknown.rawValue {
            return
        }
        
        lastLocationError = error
        stopLocationManager()
        updateLabels()
        updateGetLocationButton()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLocation = locations.last {
            
            //evitamos que el location manager nos entregue locations guardadas en cache, si la location fue determinada hace mas de cinco segundos la desechamos
            if lastLocation.timestamp.timeIntervalSinceNow < -5 {
                return
            }
            
            if lastLocation.horizontalAccuracy < 0 {
                return
            }
            
            //cuando no podemos obtener una location con una exactitud menor o igual a 10 metros, debemos calcular la distancia entre la location actual y la ultima location
            var distance = CLLocationDistance(Double.greatestFiniteMagnitude)
            if let location = currentLocation {
                distance = lastLocation.distance(from: location)
            }
            
            if currentLocation == nil || lastLocation.horizontalAccuracy < currentLocation!.horizontalAccuracy {
                currentLocation = lastLocation
                lastLocationError = nil
                
                if lastLocation.horizontalAccuracy <= locationManager.desiredAccuracy {
                    stopLocationManager()
                    updateLabels()
                    updateGetLocationButton()
                    
                    //si hay una distancia mayor a cero entre el current location y el last location debemos calcuar nuevamenta el reverse geoconding
                    if distance > 0 {
                        performingReverseGeocoding = false
                    }
                }
                
                //Reverse Geocoding
                if !performingReverseGeocoding {
                    print("*** Going to geocode")
                    performingReverseGeocoding = true
                    geocoder.reverseGeocodeLocation(lastLocation, completionHandler: {[unowned self] (placemarks, error) in
                        
                        self.lastGeocodingError = error
                        if error == nil, let p = placemarks, !p.isEmpty {
                            if self.placemark == nil {
                                //play sound
                                print("*** must sound")
                                self.playSoundEffect()
                            }
                            self.placemark = p.last
                            
                        }else {
                            self.placemark = nil
                        }
                        
                        self.performingReverseGeocoding = false
                        self.updateLabels()
                    })
                }
            //si la distancia es inferior a 1 nos indica que los dos location's estan muy cerca
            }else if distance < 1{
                
                let timeInterval = lastLocation.timestamp.timeIntervalSince(currentLocation!.timestamp)
                //si el tiempo entre el current location y last location es mayor a 10 quiere decir que las ultimas location's, estan apuntando al mismo lugar, por lo cual podemos determinar que ese lugar es el indicado a mostrar.
                if timeInterval > 10 {
                    stopLocationManager()
                    updateLabels()
                    updateGetLocationButton()
                }
            }
        }
    }
    
}


//MARK: -> Prepare Segue and sound effect
extension CurrentLocationVC {
    
    struct IDS {
        static let tagLocation = "TagLocationSegue"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.tagLocation {
            let navigationController = segue.destination as! UINavigationController
            let vc = navigationController.topViewController as! TagLocationVC
            vc.placemark = placemark
            vc.coordinate = CLLocationCoordinate2D(
                latitude: currentLocation!.coordinate.latitude,
                longitude: currentLocation!.coordinate.longitude
            )
            vc.managedObjectContext = managedObjectContext
        }
    }
    
    //Sound
    func loadSoundEffect(_ name: String) {
     
        if let path = Bundle.main.path(forResource: name, ofType: nil) {
            
            let fileURL = URL(fileURLWithPath: path, isDirectory: false)
            let error = AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
            
            if error != kAudioServicesNoError {
                print("Error loading the sound \(error)")
            }
        }
    }
    
    func unloadSoundEffect() {
        AudioServicesDisposeSystemSoundID(soundID)
        soundID = 0
    }
    
    func playSoundEffect() {
        AudioServicesPlaySystemSound(soundID)
    }
}



















