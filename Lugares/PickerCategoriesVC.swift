//
//  PickerCategoriesVC.swift
//  Lugares
//
//  Created by joan mazo on 10/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit

protocol PickerCategoriesVCDelegate: class {
    func pickerCategoriesDelegate(_ controller: PickerCategoriesVC, categorySelected: String)
}

class PickerCategoriesVC: UITableViewController {
    
    struct IDS {
        static let cateogryCell = "CategoryCell"
    }
    
    var selectedCategoryName = ""
    var selectedIndexPath: IndexPath?
    var delegate: PickerCategoriesVCDelegate?
    
    let categories = [
        "No Category",
        "Apple Store",
        "Bar",
        "Bookstore",
        "Club",
        "Grocery Store",
        "Historic Building",
        "House",
        "Icecream Vendor",
        "Landmark",
        "Park"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.black
        tableView.separatorColor = UIColor(white: 1.0, alpha: 0.2)
        tableView.indicatorStyle = .white
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let category = categories[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: IDS.cateogryCell, for: indexPath)
        cell.textLabel?.text = category
        cell.backgroundColor = UIColor.black
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.highlightedTextColor = cell.textLabel?.textColor
        
        let selectionView = UIView(frame: CGRect.zero)
        selectionView.backgroundColor = UIColor(white: 1.0, alpha: 0.2)
        cell.selectedBackgroundView = selectionView
        
        if selectedCategoryName == category {
            cell.accessoryType = .checkmark
            selectedIndexPath = indexPath
        }else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndexPath != nil, let oldCheckmarkCell = tableView.cellForRow(at: selectedIndexPath!) {
            oldCheckmarkCell.accessoryType = .none
        }
        
        if let newCheckmarkCell = tableView.cellForRow(at: indexPath) {
            newCheckmarkCell.accessoryType = .checkmark
        }
        
        let category = categories[indexPath.row]
        selectedCategoryName = category
        selectedIndexPath = indexPath
        
        delegate?.pickerCategoriesDelegate(self, categorySelected: category)
    }
    
}









