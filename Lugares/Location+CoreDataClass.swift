//
//  Location+CoreDataClass.swift
//  Lugares
//
//  Created by joan mazo on 11/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import Foundation
import CoreData
import MapKit

@objc(Location)
public class Location: NSManagedObject, MKAnnotation {
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    public var title: String? {
        if locationDescription.isEmpty {
            return "(No description)"
        }else {
            return locationDescription
        }
    }
    
    public var subtitle: String? {
        return category
    }
}
