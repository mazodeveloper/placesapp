//
//  MyNavigationController.swift
//  Lugares
//
//  Created by joan mazo on 16/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit

class MyNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
