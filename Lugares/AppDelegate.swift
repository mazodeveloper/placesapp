//
//  AppDelegate.swift
//  Lugares
//
//  Created by joan mazo on 9/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = UIColor.black
        UINavigationBar.appearance().tintColor = UIColor(red: 255/255.0, green: 238/255.0, blue: 136/255.0, alpha: 1.0)
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().isTranslucent = true
        
        //Status bar
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Tab bar appearance
        UITabBar.appearance().barTintColor = UIColor.black
        UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 238/255.0, blue: 136/255.0, alpha: 1.0)
        
        let tabBar = window?.rootViewController as! UITabBarController
        if let views = tabBar.viewControllers {
            let currentLocationVC = views[0] as! CurrentLocationVC
            currentLocationVC.managedObjectContext = managedObjectContext
            
            let navigationController = views[1] as! UINavigationController
            let locationsVC = navigationController.topViewController as! LocationsVC
            locationsVC.managedObjectContext = managedObjectContext
            let _ = locationsVC.view
            
            let mapVC = views[2] as! MapVC
            mapVC.managedObjectContext = managedObjectContext
        }
        
        print(applicationDocumentsDirectory)
        listenForFatalCoreDataNotifications()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //CoreData Configuration
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Could load data store: \(error)")
            }
        })
        
        return container
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = self.persistentContainer.viewContext
    //Fin CoreData configuration
    
    //Notification Center
    func listenForFatalCoreDataNotifications() {
        
        NotificationCenter.default.addObserver(
            forName: ManagedObjectContextSaveDidFailNotification,
            object: nil,
            queue: OperationQueue.main) {[unowned self] (notification) in
                
                let alert = UIAlertController(title: "Internal error", message: "There was a fatal error in the app and it cannot continue.\n\n Press OK to terminate the app. Sorry for the inconvenience.", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    
                    let exception = NSException(
                        name: .internalInconsistencyException,
                        reason: "Fatal CoreData error",
                        userInfo: nil
                    )
                    
                    exception.raise()
                })
                
                alert.addAction(action)
                
                self.viewControllerForShowingAlert().present(alert, animated: true, completion: nil)
        }
    }
    
    func viewControllerForShowingAlert() -> UIViewController {
        let rootVC = window!.rootViewController!
        if let presentedVC = rootVC.presentedViewController {
            return presentedVC
        }else {
            return rootVC
        }
    }
    //NotificationCenter Finished
    
}












