//
//  MyTabBarVC.swift
//  Lugares
//
//  Created by joan mazo on 16/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import Foundation
import UIKit

class MyTabBarVC: UITabBarController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        return nil
    }
    
}
