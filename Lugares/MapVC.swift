//
//  MapVC.swift
//  Lugares
//
//  Created by joan mazo on 12/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MapVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var managedObjectContext: NSManagedObjectContext! {
        didSet {
            //Core data envia notificaciones cada vez que se modifica el data store, por lo tanto podemos escuchar dichas notificaciones y modificar nuestra vista.
            NotificationCenter.default.addObserver(
                forName: Notification.Name.NSManagedObjectContextObjectsDidChange,
                object: managedObjectContext,
                queue: OperationQueue.main) {[unowned self] (notification) in
                    
                    if let dictionary = notification.userInfo {
                        print(dictionary["inserted"] ?? "Nothing inserted")
                        print(dictionary["deleted"] ?? "Nothing deleted")
                        print(dictionary["updated"] ?? "Nothing updated")
                        
                        //Location insert
                        if let locations = dictionary[NSInsertedObjectsKey] as? Set<Location> {
                            let locationInserted = locations.first!
                            if self.isViewLoaded {
                                self.mapView.addAnnotation(locationInserted)
                                self.locations.append(locationInserted)
                            }
                        }
                        
                        //Location delete
                        if let locations = dictionary[NSDeletedObjectsKey] as? Set<Location> {
                            let locationDelete = locations.first!
                            if self.isViewLoaded {
                                self.mapView.removeAnnotation(locationDelete)
                                if let index = self.locations.index(of: locationDelete){
                                    self.locations.remove(at: index)
                                }
                            }
                        }
                        
                        //Location Update
                        if let locations = dictionary[NSUpdatedObjectsKey] as? Set<Location> {
                            let locationUpdate = locations.first!
                            if self.isViewLoaded {
                                self.mapView.deselectAnnotation(locationUpdate, animated: false)
                            }
                        }
                        
                    }
            }
        }
    }
    var locations = [Location]()
    
    struct IDS {
        static let editLocation = "EditLocationSinceMap"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLocations()
        mapView.delegate = self
        
        if !locations.isEmpty {
            showLocations()
        }
    }
    
    func updateLocations() {
        mapView.removeAnnotations(locations)
        let fetchRequest = NSFetchRequest<Location>()
        fetchRequest.entity = Location.entity()
        
        do {
            locations = try managedObjectContext.fetch(fetchRequest)
            //Para que las locations puedan ser aceptadas como MKAnnotations deben implementar el protocolo MKAnnotation
            mapView.addAnnotations(locations)
        }catch let error {
            fatalCoreDataError(error)
        }
        
    }
    
    @IBAction func showUser(_ sender: Any) {
        let region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 1000, 1000)
        //el mapView.regionThatFits se encarga de hacer un zoom a la region que le hemos especificado.
        mapView.setRegion(mapView.regionThatFits(region), animated: true)
    }
    
    @IBAction func showLocations() {
        let tmp = region(for: locations)
        mapView.setRegion(mapView.regionThatFits(tmp), animated: true)
    }
    
    //calcula una region en la que se pueden ubicar la mayoria de MKAnnotations
    func region(for annotations: [MKAnnotation]) -> MKCoordinateRegion {
        
        let region: MKCoordinateRegion
        switch annotations.count {
        case 0:
            region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 1000, 1000)
        case 1:
            let annotation = annotations[annotations.count - 1]
            region = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 1000, 1000)
        default:
            
            var topLeftCoord = CLLocationCoordinate2D(latitude: -90, longitude: 180)
            var bottomRightCoord = CLLocationCoordinate2D(latitude: 90, longitude: -180)
            
            for annotation in annotations {
                topLeftCoord.latitude = max(topLeftCoord.latitude, annotation.coordinate.latitude)
                topLeftCoord.longitude = min(topLeftCoord.longitude, annotation.coordinate.longitude)
                
                bottomRightCoord.latitude = min(bottomRightCoord.latitude, annotation.coordinate.latitude)
                bottomRightCoord.longitude = max(bottomRightCoord.longitude, annotation.coordinate.longitude)
            }
            
            let center = CLLocationCoordinate2D(
                latitude: topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) / 2,
                longitude: topLeftCoord.longitude - (topLeftCoord.longitude - bottomRightCoord.longitude) / 2
            )
            
            let extraSpace = 1.1
            let span = MKCoordinateSpan(
                latitudeDelta: abs(topLeftCoord.latitude - bottomRightCoord.latitude) * extraSpace,
                longitudeDelta: abs(topLeftCoord.longitude - bottomRightCoord.longitude) * extraSpace
            )
            
            region = MKCoordinateRegion(center: center, span: span)
        }
        
        return region
    }
    
}

//MARK: ->MKMapViewDelegate
extension MapVC: MKMapViewDelegate {
    
    //Con este metodo creamos custom MKAnnotation, podemos cambiarles el aspecto y agregar actions
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard annotation is Location else {
            return nil
        }
        
        let identifier = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            pinView.isEnabled = true
            //si la annotation puede mostar un pop up con informacion
            pinView.canShowCallout = true
            pinView.animatesDrop = false
            pinView.pinTintColor = UIColor(red: 0.32, green: 0.82, blue: 0.4, alpha: 1)
            pinView.tintColor = UIColor(white: 0.0, alpha: 0.6)
            
            //right button
            let rightButton = UIButton(type: .detailDisclosure)
            rightButton.addTarget(self, action: #selector(showLocationDetail), for: .touchUpInside)
            pinView.rightCalloutAccessoryView = rightButton
            
            //image
            let tmp = annotation as! Location
            if tmp.hasPhoto {
                if let locationImage = tmp.photoImage {
                    let thumbnail = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 25)))
                    thumbnail.setBackgroundImage(
                        locationImage.resizedImage(withBounds: CGSize(width: 40, height: 25)),
                        for: .normal
                    )
                    pinView.leftCalloutAccessoryView = thumbnail
                }
            }
            
            annotationView = pinView
        }
        
        if let annotationView = annotationView {
            annotationView.annotation = annotation
            
            let button = annotationView.rightCalloutAccessoryView as! UIButton
            if let index = locations.index(of: annotation as! Location) {
                button.tag = index
            }
            
            //image
            let tmp = annotation as! Location
            if tmp.hasPhoto, let locationImage = tmp.photoImage {
                let thumbnail = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 25)))
                thumbnail.setBackgroundImage(
                    locationImage.resizedImage(withBounds: CGSize(width: 40, height: 25)),
                    for: .normal
                )
                annotationView.leftCalloutAccessoryView = thumbnail
            }
            
        }
        
        return annotationView
    }
    
    
    func showLocationDetail(_ sender: UIButton) {
        print("i need show a print with the index location: \(sender.tag)")
        performSegue(withIdentifier: IDS.editLocation, sender: sender.tag)
    }
    
    
    //prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.editLocation {
            let navigationController = segue.destination as! UINavigationController
            let tagLocation = navigationController.topViewController as! TagLocationVC
            let location = locations[sender as! Int]
            tagLocation.editLocation = location
            tagLocation.managedObjectContext = managedObjectContext
        }
    }
    
}





















