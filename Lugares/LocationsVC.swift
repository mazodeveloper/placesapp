//
//  LocationsVC.swift
//  Lugares
//
//  Created by joan mazo on 11/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class LocationsVC: UITableViewController {
    
    var managedObjectContext: NSManagedObjectContext!
    lazy var fetchedResultsController: NSFetchedResultsController<Location> = { [unowned self] in
       
        let fetchRequest = NSFetchRequest<Location>()
        fetchRequest.entity = Location.entity()
        let categorySort = NSSortDescriptor(key: "category", ascending: true)
        let dateSort = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [categorySort, dateSort]
    
        fetchRequest.fetchBatchSize = 20
        
        //el sectionNameKeyPath nos permite agrupar en sections las location's por cualquier atributo que le especifiquemos en este caso es el atributo category.
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: self.managedObjectContext,
            sectionNameKeyPath: "category",
            cacheName: "Locations"
        )
    
    fetchedResultsController.delegate = self
    
    return fetchedResultsController
    }()
    
    struct IDS {
        static let locationCell = "LocationCell"
        static let editLocation =  "EditLocationSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.black
        tableView.separatorColor = UIColor(white: 1.0, alpha: 0.2)
        tableView.indicatorStyle = .white
        
        performFetch()
    }
    
    func performFetch() {
        do {
            try fetchedResultsController.performFetch()
        }catch let error {
            fatalCoreDataError(error)
        }
    }
    
    //prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.editLocation {
            let navigationController = segue.destination as! UINavigationController
            let tagLocationVC = navigationController.topViewController as! TagLocationVC
            tagLocationVC.managedObjectContext = managedObjectContext
            
            if let cell = sender as? UITableViewCell {
                if let indexPath = tableView.indexPath(for: cell) {
                    tagLocationVC.editLocation = fetchedResultsController.object(at: indexPath)
                }
            }
        }
    }
    
    deinit {
        fetchedResultsController.delegate = nil
    }
    
}


//MARK: -> UITableViewDatasource y delegate
extension LocationsVC {
    
    //sections
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections!.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.name.uppercased()
    }
    
    //rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IDS.locationCell, for: indexPath) as! LocationCell
        let location = fetchedResultsController.object(at: indexPath)
        
        cell.updateUI(location: location)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Eliminar") {[unowned self] (action, indexPath) in
            
            let location = self.fetchedResultsController.object(at: indexPath)
            
            //if location have image try to delete the image
            location.removePhotoFile()
            
            self.managedObjectContext.delete(location)
            
            do {
                try self.managedObjectContext.save()
            }catch let error {
                fatalCoreDataError(error)
            }
        }
        
        return [deleteAction]
    }
    
    //Custom Header sections
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let labelRect = CGRect(
            x: 15,
            y: tableView.sectionHeaderHeight + 13,
            width: 300,
            height: 14
        )
        let label = UILabel(frame: labelRect)
        label.font = UIFont.boldSystemFont(ofSize: 12)
        
        label.text = tableView.dataSource!.tableView!(tableView, titleForHeaderInSection: section)
        label.textColor = UIColor(white: 1.0, alpha: 0.4)
        label.backgroundColor = UIColor.clear
        
        let separatorRect = CGRect(
            x: 15,
            y: tableView.sectionHeaderHeight + 30,
            width: tableView.bounds.size.width - 15,
            height: 0.5
        )
        
        let separator = UIView(frame: separatorRect)
        separator.backgroundColor = tableView.separatorColor
        
        let viewRect = CGRect(
            x: 0,
            y: 0,
            width: tableView.bounds.size.width,
            height: tableView.sectionHeaderHeight
        )
        
        let view = UIView(frame: viewRect)
        view.backgroundColor = UIColor(white: 0, alpha: 0.85)
        view.addSubview(label)
        view.addSubview(separator)
        
        return view
    }
    
    
}


//MARK: -> NSfetchedResultsControllerDelegate
extension LocationsVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("*** controllerWillChangeContent")
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            if let cell = tableView.cellForRow(at: indexPath!) as? LocationCell {
                let location = controller.object(at: indexPath!) as! Location
                cell.updateUI(location: location)
            }
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .update:
            print("NSFetchedResultsChangeUpdate (section)")
        case .move:
            print("NSFetchedResultsChangeMove (section)")
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("*** ControllerDidChangeContent")
        tableView.endUpdates()
    }
    
}


















