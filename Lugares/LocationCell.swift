//
//  LocationCell.swift
//  Lugares
//
//  Created by joan mazo on 11/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.black
        descriptionLabel.textColor = UIColor.white
        descriptionLabel.highlightedTextColor = descriptionLabel.textColor
        addressLabel.textColor = UIColor(white: 1.0, alpha: 0.4)
        addressLabel.highlightedTextColor = addressLabel.textColor
        
        locationImage.layer.cornerRadius = locationImage.bounds.size.width / 2
        locationImage.clipsToBounds = true
        separatorInset = UIEdgeInsets(top: 0, left: 82, bottom: 0, right: 0)
        
        //cuando el usuario de tap en una cell se pondra en el top de la cell una view de color gris para simular la seleccion
        let selectionView = UIView(frame: CGRect.zero)
        selectionView.backgroundColor = UIColor(white: 1.0, alpha: 0.2)
        selectedBackgroundView = selectionView
    }
    
    func updateUI(location: Location) {
        
        descriptionLabel.text = location.locationDescription
        addressLabel.text = LocationHelper.convertDateToString(date: location.date)
        if location.hasPhoto, let image = location.photoImage {
            locationImage.image = image.resizedImage(withBounds: CGSize(width: 52, height: 52))
            locationImage.isHidden = false
        }else {
            locationImage.image = UIImage(named: "No Photo")!
        }
    }

}









