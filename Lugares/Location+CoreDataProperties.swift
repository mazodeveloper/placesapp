//
//  Location+CoreDataProperties.swift
//  Lugares
//
//  Created by joan mazo on 11/05/17.
//  Copyright © 2017 joan mazo. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation
import UIKit

extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var date: Date
    @NSManaged public var locationDescription: String
    @NSManaged public var category: String
    @NSManaged public var placemark: CLPlacemark?
    @NSManaged public var photoID: NSNumber?
    
    var hasPhoto: Bool {
        return photoID != nil
    }

    var photoURL: URL {
        //los assert's nos permiten hacer validaciones, en caso de que la validacion falle la app will crash y mostrara en debug el mensaje que le especifiquemos
        assert(photoID != nil, "No photoID set")
        let filename = "Photo-\(photoID!.intValue).jpg"
        //agegar un / mas junto al nombre del archivo
        return applicationDocumentsDirectory.appendingPathComponent(filename)
    }
    
    var photoImage: UIImage? {
        return UIImage(contentsOfFile: photoURL.path)
    }
    
    class func nextPhotoID() -> Int {
        let userDefault = UserDefaults.standard
        let currentID = userDefault.integer(forKey: "PhotoID")
        userDefault.set(currentID + 1, forKey: "PhotoID")
        userDefault.synchronize()
        return currentID
    }
    
    func removePhotoFile() {
        if hasPhoto {
            do {
                try FileManager.default.removeItem(at: photoURL)
            }catch let error {
                print("Error removing file: \(error)")
            }
        }
    }
    
}










